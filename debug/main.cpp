#include <iostream>
#include <iomanip>
#include <vector>
#include <assert.h>
#include <cmath>
#include <sys/time.h>
#include <particle_simulator.hpp>

#include "EoS.h"
#include "class_platform.hpp"
#include "init/GI.h"

PS::S32 DensDispatchKernel(const PS::S32, const PS::S32, const EPI::Dens**, const PS::S32*, const EPJ::Dens**, const PS::S32*);

PS::S32 DensRetrieveKernel(const PS::S32, const PS::S32, const PS::S32*, RESULT::Dens**);

double get_dtime(void){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 0.001 * 0.001);
}

int main(int argc, char *argv[]){
	const int N_WALK_LIMIT = 256;
	std::cout << std::scientific << std::setprecision(16);
	std::cerr << std::scientific << std::setprecision(16);
	double time[4];
	PS::Initialize(argc, argv);
	#ifdef ENABLE_PEZY
		#ifdef PARTICLE_SIMULATOR_MPI_PARALLEL
			//omp_set_num_threads(omp_get_max_threads() / PS::Comm::getNumberOfProc());
			omp_set_num_threads(4);
			std::cout << "PZ MPI" << std::endl;
		#endif
		//extern PezyDevice device;
		//device.initialize();
	#endif
	PS::DomainInfo dinfo;
	//
	PS::ParticleSystem<RealPtcl> ptcl;
	ptcl.initialize();
	//
	typedef GI_init<RealPtcl> PROBLEM;
	//
	PROBLEM::setupIC(ptcl, dinfo);
	std::cerr << ptcl.getNumberOfParticleGlobal() << std::endl;
	for(int i = 0 ; i < ptcl.getNumberOfParticleLocal() ; ++ i){
		ptcl[i].setPressure();
		ptcl[i].initialize();
	}

	int Nlf, Ngrp;
	Nlf = 8;
	Ngrp = 128;
	const int loop = 16;

	dinfo.initialize(0.3);
	PS::TreeForForceShort<RESULT::Dens, EPI::Dens, EPJ::Dens>::Gather dens_tree;
	dens_tree.initialize(ptcl.getNumberOfParticleLocal(), 0.5, Nlf, Ngrp);
	std::cout << "# of threads: " << PS::Comm::getNumberOfThread() << std::endl;

	for(int l = 0 ; l < loop ; ++ l){
		time[0] = PS::GetWtime();
		dinfo.collectSampleParticle(ptcl);
		dinfo.decomposeDomainAll(ptcl);
		ptcl.exchangeParticle(dinfo);
		dens_tree.clearTimeProfile();
		#ifdef ENABLE_PEZY
			dens_tree.calcForceAllAndWriteBackMultiWalk(DensDispatchKernel, DensRetrieveKernel, 1, ptcl, dinfo, N_WALK_LIMIT, true);
		#else
			dens_tree.calcForceAllAndWriteBack(CalcDensity(), ptcl, dinfo);
		#endif
		time[0] = PS::Comm::getMaxValue(dens_tree.getTimeProfile().getTotalTime());
		if(PS::Comm::getRank() == 0){
			std::cout << "dens tree ===================" << std::endl;
			dens_tree.getTimeProfile().dump();
			std::cout << "=============================" << std::endl;
		}

		if(PS::Comm::getRank() == 0) std::cout << l << " " << time[0] << " " << time[1] << " " << time[2] << std::endl;
	}
	ptcl.writeParticleAscii("./result.txt");
	PS::Finalize();
	return 0;
}

PS::S32 DensDispatchKernel(const PS::S32 tag, const PS::S32 n_walk, const EPI::Dens* epi[], const PS::S32 Nepi[], const EPJ::Dens* epj[], const PS::S32 Nepj[]){
	std::cout << "walk start" << std::endl;
	std::cout << "walk end" << std::endl;
	return 0;
}

PS::S32 DensRetrieveKernel(const PS::S32 tag, const PS::S32 n_walk, const PS::S32 ni[], RESULT::Dens* force[]){
	return 0;
}

