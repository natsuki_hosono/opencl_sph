#pragma once

//const int N_THREAD_GPU = 2688;
const int N_THREAD_GPU = 1024;
const int N_WALK_LIMIT = 200;
const int NI_LIMIT     = 10000 * N_WALK_LIMIT;
const int NJ_LIMIT     = 10000 * N_WALK_LIMIT;

